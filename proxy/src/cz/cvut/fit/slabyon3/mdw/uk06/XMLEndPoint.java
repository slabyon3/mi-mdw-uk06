package cz.cvut.fit.slabyon3.mdw.uk06;

public class XMLEndPoint {
    private final String address;
    private boolean available;

    public XMLEndPoint(String address, boolean available) {
        this.address = address;
        this.available = available;
    }

    public String getAddress() {
        return address;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
