package cz.cvut.fit.slabyon3.mdw.uk06;

import oracle.sysman.ccr.collector.collectionMgr.Collection;
import weblogic.servlet.annotation.WLServlet;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Collections;

@WLServlet(mapping = "/proxy", name = "ProxyServlet")
public class ProxyServlet extends HttpServlet {
    private static final XMLEndPoint[] endpoints = {
            new XMLEndPoint("http://147.32.233.18:8888/MI-MDW-LastMinute1/list", true),
            new XMLEndPoint("http://147.32.233.18:8888/MI-MDW-LastMinute2/list", true),
            new XMLEndPoint("http://147.32.233.18:8888/MI-MDW-LastMinute3/list", true)
    };
    private static int currentServer = 0;

    private static synchronized String getAvailableServerURL() throws IOException {
        int startServer = currentServer;
        do {
            endpoints[currentServer].setAvailable(
                    getAvailability(endpoints[currentServer].getAddress())
            );
            if (endpoints[currentServer].isAvailable()) {
                startServer = currentServer;
                currentServer = (currentServer + 1) % endpoints.length;
                return endpoints[startServer].getAddress();
            }
        } while ((currentServer = (currentServer + 1) % endpoints.length) != startServer);
        return null;
    }

    private static boolean getAvailability(String url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) (new URL(url)).openConnection();
        connection.setRequestMethod("GET");
        int code = connection.getResponseCode();
        System.out.println(url + " returned code: " + code);

        switch (code) {
            case 200:
                return true;
            case 500:
                return false;
            default:
                System.out.println("Unrecognized code, treating as unavailable.");
                return false;
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = null;
        try {
            url = getAvailableServerURL();
        } catch (IOException e) {
            response.getWriter().println("All servers are unavailable. Please try again later.");
            return;
        }
        if (url == null) {
            response.getWriter().println("All servers are unavailable. Please try again later.");
            return;
        }
        System.out.println("Using url: " + url);

        HttpURLConnection connection = (HttpURLConnection) (new URL(url)).openConnection();
        // HTTP method
        connection.setRequestMethod(request.getMethod());
        // copy headers
        for (Object headerName : Collections.list(request.getHeaderNames())) {
            String header = String.valueOf(headerName);
            connection.setRequestProperty(header, request.getHeader(header));
        }
        // copy body
        BufferedReader inputStream = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        ServletOutputStream sout = response.getOutputStream();
        while ((inputLine = inputStream.readLine()) != null) {
            sout.write(inputLine.getBytes());
        }
        // close
        inputStream.close();
        sout.flush();
    }
}
